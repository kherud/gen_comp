var config = {
    "crossover_ratio": 1,
    "mutation_ratio": 0.5,
    "elitism_ratio": 0,
    "population_size": 2048
};

var chars = [];
for (var i = 32; i < 127; i++)
    chars.push(String.fromCharCode(i));

var post_interval = null;
var calc_interval = null;

self.addEventListener('message', function (e) {
    var data = e.data;
    var population = get_instance(data.target);
    var candidates = [];
    var post_data = function(){
        self.postMessage({
            "candidates": candidates
        });
        candidates = [];
    };
    clearInterval(post_interval);
    clearInterval(calc_interval);
    post_interval = setInterval(function(){
        post_data();
    }, 50);
    calc_interval = setInterval(function(){
        population.evolve();
        candidates.push({
            "generation": population.generation,
            "best_gens": population.population[0]
        });
        if (population.get_fitness(population.population[0]) <= 0){
            clearInterval(post_interval);
            clearInterval(calc_interval);
            post_data();
            self.postMessage("fin");
        }
    }, 0);
}, false);

function get_instance(target) {
    var instance = {
        target: target,
        target_length: target.length,
        elitism_size: Math.floor(config.elitism_ratio * config.population_size),
        generation: 0,
        population: [],

        get_population: function () {
            this.population = [];
            for (var i = 0; i < config.population_size; i++) {
                this.population.push(this.get_candidate());
            }
        },

        get_candidate: function () {
            var candidate = "";
            for (i = 0; i < this.target_length; i++) {
                candidate += chars[rnd(chars.length)];
            }
            return candidate;
        },

        evolve: function () {
            var new_population = this.population.slice(0, this.elitism_size);
            var new_population_size = this.elitism_size;
            while (new_population_size < this.population.length) {
                var rand = Math.random();
                if (rand < config.mutation_ratio) {
                    new_population.push(this.mutate(this.population[new_population_size]));
                    new_population_size++;
                }
                if (rand < config.crossover_ratio) {
                    var mating_range = Math.random();
                    var parent1 = rnd(Math.floor(mating_range * config.population_size));
                    var parent2 = rnd(Math.floor(mating_range * config.population_size));
                    var children = this.crossover(this.population[parent1], this.population[parent2]);
                    new_population.push(children[0]);
                    new_population.push(children[1]);
                    new_population_size += 2;
                } else {
                    new_population.push(this.population[new_population_size]);
                    new_population_size++;
                }
            }
            new_population.length = config.population_size;
            new_population.sort(function (candidate1, candidate2) {
                var fitness1 = instance.get_fitness(candidate1);
                var fitness2 = instance.get_fitness(candidate2);
                return fitness1 - fitness2;
            });
            this.population = new_population;
            this.generation++;
        },

        get_fitness: function (candidate) {
            var fitness = this.target.length;
            for (var i = 0; i < this.target.length; i++) {
                if (candidate.charAt(i) === this.target.charAt(i)) {
                    fitness--;
                }
            }
            return fitness;
        },

        crossover: function (parent1, parent2) {
            var pivot = rnd(this.target_length - 1) + 1;
            var gens1 = split_string(parent1, pivot);
            var gens2 = split_string(parent2, pivot);
            return [gens2[0] + gens1[1], gens1[0] + gens2[1]];
        },

        mutate: function (parent) {
            var pivot = rnd(this.target_length + 1);
            var rnd_char = rnd(chars.length);
            return setCharAt(parent, pivot, chars[rnd_char]);
        }
    };
    instance.get_population();
    return instance;
}


function rnd(max) {
    return Math.floor(Math.random() * max);
}

function split_string(value, index) {
    return [value.substring(0, index), value.substring(index)];
}

function setCharAt(str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}
