var input = document.getElementById("target-input");
var gen_container = document.getElementById("gen-container");
var rnd_container = document.getElementById("rnd-container");
var gen_worker = new Worker('genetic_generator.js');
var rnd_worker = new Worker('random_generator.js');

gen_worker.addEventListener('message', function(e) {
    if (e.data === "fin"){
        gen_timer.stop();
    } else {
        var html = "";
        e.data.candidates.forEach(function (candidate) {
            html += candidate.generation + " - " + candidate.best_gens + "<br>";
        });
        gen_container.innerHTML = (gen_container.innerHTML + html).slice(-10000);
        gen_container.scrollTop = gen_container.scrollHeight;
    }
}, false);

rnd_worker.addEventListener('message', function (e) {
    if (e.data === "fin"){
        rnd_timer.stop();
    } else {
        var html = "";
        e.data.candidates.forEach(function (candidate) {
            html += candidate.number + " - " + candidate.candidate + "<br>";
        });
        rnd_container.innerHTML = (rnd_container.innerHTML + html).slice(-10000);
        rnd_container.scrollTop = rnd_container.scrollHeight;
    }
}, false);

function calculate() {
    reset();
    var target = input.value;
    gen_worker.postMessage({'target': target});
    rnd_worker.postMessage({'target': target});
    gen_timer.start();
    rnd_timer.start();
}

function reset(){
    gen_container.innerHTML = "";
    rnd_container.innerHTML = "";
    gen_timer.reset();
    rnd_timer.reset();
}

/* ###################################### Stopwatches ###################################### */

var Stopwatch = function(elem) {
    var timer       = elem,
        offset,
        clock = 0,
        interval;

    function start() {
        if (!interval) {
            offset   = Date.now();
            interval = setInterval(update, 10);
        }
    }

    function stop() {
        if (interval) {
            clearInterval(interval);
            interval = null;
            if (clock < 0.01) {
                timer.innerHTML = "<0.01";
            }
        }
    }

    function reset() {
        clock = 0;
        timer.innerHTML = "";
    }

    function update() {
        clock += delta();
        timer.innerHTML = (clock / 1000).toFixed(2);
    }

    function delta() {
        var now = Date.now(),
            d   = now - offset;
        offset = now;
        return d;
    }

    // public API
    this.start  = start;
    this.stop   = stop;
    this.reset  = reset;
};

gen_timer = new Stopwatch(document.getElementById("gen-timer"));
rnd_timer = new Stopwatch(document.getElementById("rnd-timer"));