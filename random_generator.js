var chars = [];
for (var i = 32; i < 127; i++)
    chars.push(String.fromCharCode(i));

var post_interval = null;
var calc_interval = null;

self.addEventListener('message', function (e) {
    var data = e.data;
    var target = data.target;
    var candidates = [];
    var number = 0;
    var post_data = function(){
        self.postMessage({
            "candidates": candidates
        });
        candidates = [];
    };
    clearInterval(post_interval);
    clearInterval(calc_interval);
    post_interval = setInterval(function(){
        post_data();
    }, 50);
    var candidate = "";
    calc_interval = setInterval(function(){
        number++;
        candidate = get_rnd(target.length);
        candidates.push({
            "candidate": candidate,
            "number": number
        });
        if (candidate === target){
            clearInterval(post_interval);
            clearInterval(calc_interval);
            post_data();
            self.postMessage("fin");
        }
    }, 0);
}, false);

function get_rnd(length) {
    var str = "";
    for (i = 0; i < length; i++) {
        str += chars[rnd(chars.length)];
    }
    return str;
}

function rnd(max) {
    return Math.floor(Math.random() * max);
}